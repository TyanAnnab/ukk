<?php 

class M_calon_peserta extends CI_Model{
	public function tampil_data()
	{
		return $this->db->get('tb_calon_peserta');
	}

	public function input_data($data)
	{
		$this->db->insert('tb_calon_peserta',$data);
	}

	public function hapus_data($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function edit_data($where, $table)
	{
	  return $this->db->get_where($table,$where);
	}

	public function update_data($where, $data,$table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function detail_data($id = NULL)
	{
		$query = $this->db->get_where('tb_calon_peserta', array('id' => $id))->row();
		return $query;
	}

}