<?php 
 
 class Murid extends CI_Controller{
 	public function index ()
 	{
 		$data['murid'] = $this->m_murid->tampil_data()->result();

 		$this->load->view('templates/header');
		$this->load->view('murid', $data);
		$this->load->view('templates/footer');

 	} 
 	public function hapus ($id)
 	{
 		$where = array ('id' => $id);
 		$this->m_murid->hapus_data($where, 'tb_murid');
 		redirect ('murid/index');
 	}

 }
 ?>