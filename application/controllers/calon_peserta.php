<?php 

 class calon_peserta extends CI_Controller{
 	public function index ()
 	{
 		$data['calon_peserta'] = $this->m_calon_peserta->tampil_data()->result();

 		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('calon_peserta', $data);
		$this->load->view('templates/footer');

 	}

 	public function tambah()
 	{
 		$this->load->view('templates/header');
 		$this->load->view('templates/sidebar');
		$this->load->view('calon_peserta', $data);
		$this->load->view('templates/footer');
 	}

 	public function tambah_aksi()
 	{
 		//$this->load->model("m_calon_peserta");
 		//$model = $this->m_calon_peserta;

 		$nama 	   = $this->input->post('nama');
 		$nim	   = $this->input->post('nim');
 		$tgl_lahir = $this->input->post('tgl_lahir');
 		$jurusan   = $this->input->post('jurusan');
 		$alamat   = $this->input->post('alamat');
 		$email   = $this->input->post('email');
 		$no_telp   = $this->input->post('no_telp');
 		$foto      = $_FILES['foto'];
 		if ($foto=''){}else{
 			$config['upload_path']  = './assets/foto';
 			$config['allowed_types'] = 'jpg|png|gif';
 			$this->load->library('upload',$config);
 			if(!$this->upload->do_upload('foto')){
 				echo "Upload Gagal"; die();
 			}else{
 				$foto=$this->upload->data('file_name');
 			}
 		}

 		$data = array (
 			'nama'        => $nama,
 			'nim'         => $nim,
 			'tgl_lahir'   => $tgl_lahir,
 			'jurusan'     => $jurusan,
 			'alamat'     => $alamat,
 			'email'     => $email,
 			'no_telp'     => $no_telp,
 			'foto'        => $foto,
 		);

 		$this->m_calon_peserta->input_data($data, 'tb_calon_peserta');
 		redirect('calon_peserta/index');
 	}


 	public function hapus ($id)
 	{
 		$where = array ('id' => $id);
 		$this->m_calon_peserta->hapus_data($where, 'tb_calon_peserta');
 		redirect ('calon_peserta/index');
 	}

 	public function edit ($id)
 	{
 		$where = array('id' => $id);
 		$data['calon_peserta'] = $this->m_calon_peserta->edit_data($where,'tb_calon_peserta')->result();

 		$this->load->view('templates/header');
 		$this->load->view('templates/sidebar');
		$this->load->view('edit', $data);
		$this->load->view('templates/footer');
 	}

 	public function update ()
 	{
 		$id = $this->input->post('id');
 		$nama = $this->input->post('nama');
 		$nim = $this->input->post('nim');
 		$tgl_lahir = $this->input->post('tgl_lahir');
 		$jurusan = $this->input->post('jurusan');
 		$alamat = $this->input->post('alamat');
 		$email = $this->input->post('email');
 		$no_telp = $this->input->post('no_telp');
 		//$foto      = $_FILES['foto'];
 		//if ($foto=''){}else{
 		//	$config['upload_path']  = './assets/foto';
 		//	$config['allowed_types'] = 'jpg|png|gif';
 		//	$this->load->library('upload',$config);
 		//	if(!$this->upload->do_upload('foto')){
 		//		echo "Upload Gagal"; die();
 		//	}else{
 		//		$foto=$this->upload->data('file_name');
 		//	}
 		//}

 		$data = array (
 			'nama'  => $nama,
 			'nim'  => $nim,
 			'tgl_lahir'  => $tgl_lahir,
 			'jurusan'  => $jurusan,
 			'alamat'     => $alamat,
 			'email'     => $email,
 			'no_telp'     => $no_telp,
 		//	'foto'       => $foto,
 		);

 		$where = array(
 			'id' => $id
 		);

 		$this->m_calon_peserta->update_data($where,$data, 'tb_calon_peserta');
 		redirect('calon_peserta/index');
 	}

 	public function detail ($id)
 	{
 		$this->load->model('m_calon_peserta');
 		$detail = $this->m_calon_peserta->detail_data($id);
 		$data['detail'] = $detail;
 		$this->load->view('templates/header');
 		$this->load->view('templates/sidebar');
		$this->load->view('detail', $data);
		$this->load->view('templates/footer');
 	}

 	


 }