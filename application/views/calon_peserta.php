<div class="content-wrapper">
	<section class="content-header">
      <h1>
        Data calon_peserta
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data calon_peserta</li>
      </ol>
    </section>

    <section class="content">
    <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i>Tambah Data calon_peserta</button> 
    	<table class='table'>
    		<tr>
    			<th>NO</th>
    			<th>NAMA calon_peserta</th>
    			<th>NIM</th>
    			<th>TANGGAL LAHIR</th>
    			<th>JURUSAN</th>
    			<th colspan="2">AKSI</th>
    		</tr>

    		<?php 

    		$no = 1;
    		foreach ($calon_peserta as $mhs) : ?>
    		<tr>
    			<td><?php echo $no++ ?></td>
    			<td><?php echo $mhs->nama ?></td>
    			<td><?php echo $mhs->nim ?></td>
    			<td><?php echo $mhs->tgl_lahir ?></td>
    			<td><?php echo $mhs->jurusan ?></td>
    			<td><?php echo anchor('calon_peserta/detail/'.$mhs->id,'<div class="btn btn-success btn-sm"><i class="fa fa-search-plus"></i></div>') ?></td>
    			<td onclick="javascript: return confirm('Anda yakin hapus?')"> <?php echo anchor('calon_peserta/hapus/'. $mhs->id, '<div class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></div>') ?></td>
    			<td><?php echo anchor('calon_peserta/edit/'.$mhs->id, '<div class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></div>') ?></td>
    		</tr>

    	<?php  endforeach; ?>
    	</table>
    </section>
 


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">FORM INPUT DATA calon_peserta</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <?php echo form_open_multipart('calon_peserta/tambah_aksi'); ?>
        	
        	<div class="form-group">
        		<label>Nama calon_peserta</label>
        		<input type="text" name="nama" class="form-control">
        	</div>
        	<div class="form-group">
        		<label>NIM</label>
        		<input type="text" name="nim" class="form-control">
        	</div>
        	<div class="form-group">
        		<label>Tanggal Lahir</label>
        		<input type="date" name="tgl_lahir" class="form-control">
        	</div>
        	<div class="form-group">
        		<label>Jurusan</label>
        		<select class="form-control" name="jurusan">
        			<option>Rekayasa Perangkat Lunak</option>
        			<option>Tata Boga</option>
        			<option>Ilmu Komunikasi</option>
        			<option>Multi Media</option>
        		</select>
        	</div>
        	<div class="form-group">
        		<label>Alamat</label>
        		<input type="text" name="alamat" class="form-control">
        	</div>
        	<div class="form-group">
        		<label>Email</label>
        		<input type="text" name="email" class="form-control">
        	</div>
        	<div class="form-group">
        		<label>No. Telepon</label>
        		<input type="text" name="no_telp" class="form-control">
        	</div>
        	<div class="form-group">
        		<label>Upload Foto</label>
        		<input type="file" name="foto" class="form-control">
        	</div>
        	<button type="reset" class="btn btn-danger" data-bs-dismiss="modal">Reset</button>
        <button type="submit" class="btn btn-primary">Simpan</button>

       <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
</div>