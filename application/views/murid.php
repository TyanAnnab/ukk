<div class="content-wrapper">
	<section class="content-header">
      <h1>
        Data PPDB
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Murid</li>
      </ol>
    </section>

    <section class="content">
      <button type="button" class="btn btn-primary"  data-bs-toggle="modal" data-bs-target="#exampleModal">
<i class="fa fa-plus"></i> Tambah Data Murid</button>
      <table class="table">
        <tr>
          <th>NO</th>
          <th>Nama Murid</th>
          <th>NISN</th>
          <th>Asal Sekolah</th>
          <th>Alamat</th>
        </tr>


        <?php 
        $no = 1;
        foreach ($murid as $m) : 
         ?>

        <tr>
          <td><?php echo $no++ ?></td>
          <td><?php echo $m->nama ?></td>
          <td><?php echo $m->nisn ?></td>
          <td><?php echo $m->asal_sekolah ?></td>
          <td><?php echo $m->alamat ?></td>
          <td onclick="javascript: return confirm('Anda yakin hapus?')"> <?php echo anchor('murid/hapus/'. $m->id, '<div class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></div>') ?></td>
          <td><div class="btn btn-primary"><i class="fa fa-edit"></i></div></td>
        </tr>
      <?php endforeach; ?>
      </table>
    </section>





<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

</div>